#!/bin/bash

if [ "`uname -m`" == "x86_64" ]; then
  LIB="linux64"
else
  LIB="linux"
fi

PREFIX="/media/data/blender_"$(whoami)
#PREFIX_SRC=$PREFIX/blender
PREFIX_BIN=$PREFIX/cmake_debug/bin
#export BLENDER_USER_SCRIPTS=/d/pro/scripts

# export SDL_AUDIODRIVER=alsa
# export LD_LIBRARY_PATH=/shared/software/exr/lib 
echo "set print thread-events off" > /tmp/blender_gdb_cmd
echo "r" >> /tmp/blender_gdb_cmd
echo "bt" >> /tmp/blender_gdb_cmd # generate a backtrace
cd $PREFIX_BIN

#export LD_LIBRARY_PATH=/shared/software/openjpeg1_5/lib:/shared/software/ocio_git/install/lib:/shared/software/oiio_git/install/lib
export LD_LIBRARY_PATH=/shared/software/openjpeg1_5/lib:/shared/software/ocio_git/install/lib

if [ "$BLENDER_CONSOLE" = "TRUE" ] ; then
    echo "q" >> /tmp/blender_gdb_cmd # finish
    #LD_PRELOAD=/shared/software/jemalloc/lib/libjemalloc.so LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:/shared/software/exr/lib  gdb -quiet -x /tmp/blender_gdb_cmd --args blender_debug $@
    LD_PRELOAD=/shared/bin/lib/$LIB/jemalloc/lib/libjemalloc.so  gdb -quiet -x /tmp/blender_gdb_cmd --args ./blender $@
else
    #`dirname $0`/terminal.sh "" "LD_PRELOAD=/shared/software/jemalloc/lib/libjemalloc.so LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:/shared/software/exr/lib  gdb -quiet -x /tmp/blender_gdb_cmd --args blender_debug $@"
    `dirname $0`/terminal.sh "" "LD_PRELOAD=/shared/bin/lib/$LIB/jemalloc/lib/libjemalloc.so  gdb -quiet -x /tmp/blender_gdb_cmd --args ./blender $@"
fi
