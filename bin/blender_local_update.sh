#!/bin/bash

PREFIX="/media/data/blender_"$(whoami)
#~ BRANCH="master"
BRANCH="gooseberry"


SRC="blender_$BRANCH"

PREFIX_SRC=$PREFIX/$SRC

if [ "`uname -m`" == "x86_64" ]; then
	LIB="linux64"
	OPENAL="/shared/software/openal"
	DO_OPENCOLOR="ON"
	DO_OPENJPEG="ON"
else
	LIB="linux"
	OPENAL="/usr"
	DO_OPENCOLOR="OFF"
	DO_OPENJPEG="OFF"
fi


if [ ! -d $PREFIX ]; then
	mkdir -p $PREFIX
fi
if [ ! -d $PREFIX ]; then
	echo "Could not create blender dir!" $PREFIX
	sleep 2s
	exit 0
fi

#if [ ! -d /usr/local/cuda ]; then
#	echo "Installing CUDA and needed tools..."
#	sudo /shared/bin/config-script/setup-cuda.sh
#fi

cd $PREFIX

rm -rf install

if [ ! -d $PREFIX_SRC ]; then
	# creates PREFIX_SRC
	git clone http://git.blender.org/blender.git $SRC
	if [ "x$BRANCH" == "x" ]; then
		# svn checkout https://svn.blender.org/svnroot/bf-blender/trunk/blender $SRC
		echo "using master..."
	else
		# svn checkout https://svn.blender.org/svnroot/bf-blender/branches/$BRANCH $SRC
		cd $SRC; git checkout $BRANCH ; cd -
	fi
	cd $SRC
else
	cd $SRC
	# svn revert source/blender/editors/datafiles/splash.png.c
	# svn update
	#svn revert -R .
	git pull
fi

git checkout $BRANCH
git submodule update --init --recursive
git submodule foreach git checkout master
git submodule foreach git pull --rebase origin master

#if [ "x$BRANCH" == "x" ]; then
#	if [ -f "/shared/mango/splash.png.c" ]; then
#		cp -f "/shared/mango/splash.png.c" $PREFIX_SRC/source/blender/editors/datafiles/
#	fi
#fi

# export LD_LIBRARY_PATH=/shared/software/exr/lib

CORES=`cat /proc/cpuinfo | grep cores | uniq | sed -e "s/.*: *\(.*\)/\\1/"`
#CORES=4

## CMAKE!!! CAKE
if [ ! -d ../cmake_release ]; then
	mkdir ../cmake_release
else
	rm -f ../cmake_release/CMakeCache.txt
fi
if [ ! -d ../cmake_debug ]; then
	mkdir ../cmake_debug
else
	rm -f ../cmake_debug/CMakeCache.txt
fi

if [ -d /opt/oiio-1.0 ]; then
	OIIO="/opt/oiio-1.0"
else
	OIIO="/shared/software/oiio-1.1.2"
fi

#   -D WITH_OPENAL:BOOL=OFF \
# -ffast-math is slower on multiple systems. go figure! - campbell
cd ../cmake_release
cmake \
    -D CMAKE_BUILD_TYPE:STRING=Release \
    -D CMAKE_C_FLAGS:STRING="-march=native -O2 -mtune=native -fomit-frame-pointer -msse -msse2 -msse3 -ftree-vectorize -finline-functions -funswitch-loops --fast-math -fvisibility=hidden -pipe" \
    -D CMAKE_CXX_FLAGS:STRING="-march=native -O2 -mtune=native -fomit-frame-pointer -msse -msse2 -msse3 -ftree-vectorize -finline-functions -funswitch-loops --fast-math -fvisibility=hidden -pipe" \
    -D CMAKE_VERBOSE_MAKEFILE:BOOL=OFF \
    -D CMAKE_COLOR_MAKEFILE:BOOL=ON \
    -D WITH_INSTALL:BOOL=OFF \
    -D WITH_BUILDINFO:BOOL=ON \
    -D WITH_DDS:BOOL=OFF \
    -D WITH_OPENMP:BOOL=ON \
    -D WITH_JACK:BOOL=OFF \
    -D WITH_OPENJPEG:BOOL=ON \
    -D WITH_GAMEENGINE:BOOL=ON \
    -D WITH_OPENCOLLADA:BOOL=OFF \
    -D WITH_CODEC_FFMPEG:BOOL=ON \
    -D FFMPEG:BOOL=/opt/lib/ffmpeg \
    -D FFMPEG_LIBRARIES='avformat;avcodec;avutil;avdevice;swscale;rt;theoradec;theoraenc;theora;vorbisfile;vorbis;vorbisenc;ogg;xvidcore;vpx;mp3lame;x264;openjpeg_JPWL;openjpeg' \
    -D WITH_OPENAL:BOOL=ON \
    -D WITH_FFTW3:BOOL=ON \
    -D OPENEXR_INCLUDE_DIR:PATH=/opt/lib/openexr/include/OpenEXR \
    -D WITH_PYTHON_INSTALL_NUMPY:BOOL=OFF \
    -D OPENAL_INCLUDE_DIR:PATH=$OPENAL/include \
    -D OPENAL_LIBRARY:FILEPATH=$OPENAL/lib/libopenal.so \
    -D CMAKE_CXX_COMPILER=/usr/bin/g++ \
    -D CMAKE_C_COMPILER=/usr/bin/gcc \
    -D WITH_CYCLES_CUDA_BINARIES=OFF\
    ../$SRC

#    -D FFMPEG:BOOL=/shared/software/ffmpeg-1.0.1 \
#    -D FFMPEG_LIBRARIES="avdevice;avformat;avcodec;avutil;avfilter;swscale;swresample;postproc" \
#    -D OPENJPEG_LIBRARY:FILEPATH=/shared/software/openjpeg1_5/lib/libopenjpeg.so \
#    -D OPENJPEG_INCLUDE_DIR:FILEPATH=/shared/software/openjpeg1_5/include/openjpeg-1.5 \
#    -D OPENIMAGEIO_ROOT_DIR=$OIIO \
#    -D WITH_OPENCOLORIO=$DO_OPENCOLOR \
#    -D OPENCOLORIO_ROOT_DIR="/shared/software/ocio_git/install" \
#    -D BOOST_ROOT=/shared/software/boost-1.52  \
#    -D Boost_NO_SYSTEM_PATHS=ON \
#    -D Boost_USE_STATIC_LIBS=ON \
#    -D PYTHON_ROOT_DIR=/shared/software/python-3.3.0 \
#    -D PYTHON_INCLUDE_DIR=/shared/software/python-3.3.0/include/python3.3m \
#    -D PYTHON_INCLUDE_DIRS=/shared/software/python-3.3.0/include/python3.3m \
#    -D PYTHON_LIBPATH=/shared/software/python-3.3.0/lib/ \

make -s -j $CORES install

#cd ../blender ; mv ../cmake_release/bin/blender ./blender
strip -s ../cmake_release/bin/blender

# exit 0 # testing!!!

# difference is only CMAKE_BUILD_TYPE, OPENMP, CMAKE_C_FLAGS, CMAKE_CXX_FLAGS
cd ../cmake_debug
cmake \
    -D CMAKE_BUILD_TYPE:STRING=Debug \
    -D CMAKE_VERBOSE_MAKEFILE:BOOL=OFF \
    -D CMAKE_COLOR_MAKEFILE:BOOL=ON \
    -D WITH_INSTALL:BOOL=OFF \
    -D WITH_BUILDINFO:BOOL=ON \
    -D WITH_DDS:BOOL=OFF \
    -D WITH_JACK:BOOL=OFF \
    -D WITH_OPENJPEG:BOOL=ON \
    -D WITH_GAMEENGINE:BOOL=ON \
    -D WITH_OPENCOLLADA:BOOL=OFF \
    -D WITH_OPENMP:BOOL=OFF \
    -D WITH_CODEC_FFMPEG:BOOL=ON \
    -D FFMPEG:BOOL=/opt/lib/ffmpeg \
    -D FFMPEG_LIBRARIES='avformat;avcodec;avutil;avdevice;swscale;rt;theoradec;theoraenc;theora;vorbisfile;vorbis;vorbisenc;ogg;xvidcore;vpx;mp3lame;x264;openjpeg_JPWL;openjpeg' \
    -D WITH_OPENAL:BOOL=ON \
    -D WITH_FFTW3:BOOL=ON \
    -D OPENEXR_INCLUDE_DIR:PATH=/opt/lib/openexr/include/OpenEXR \
    -D WITH_PYTHON_INSTALL_NUMPY:BOOL=OFF \
    -D OPENAL_INCLUDE_DIR:PATH=$OPENAL/include \
    -D OPENAL_LIBRARY:FILEPATH=$OPENAL/lib/libopenal.so \
    -D CMAKE_CXX_COMPILER=/usr/bin/g++ \
    -D CMAKE_C_COMPILER=/usr/bin/gcc \
    -D WITH_CYCLES_CUDA_BINARIES=OFF \
    ../$SRC

#    -D FFMPEG_LIBRARIES="avdevice;avformat;avcodec;avutil;avfilter;swscale;swresample;postproc" \
#    -D OPENJPEG_LIBRARY:FILEPATH=/shared/software/openjpeg1_5/lib/libopenjpeg.so \
#    -D OPENJPEG_INCLUDE_DIR:FILEPATH=/shared/software/openjpeg1_5/include/openjpeg-1.5 \
#    -D OPENIMAGEIO_ROOT_DIR=$OIIO \
#    -D WITH_OPENCOLORIO=$DO_OPENCOLOR \
#    -D OPENCOLORIO_ROOT_DIR="/shared/software/ocio_git/install" \
#    -D BOOST_ROOT=/shared/software/boost-1.52  \
#    -D Boost_NO_SYSTEM_PATHS=ON \
#    -D Boost_USE_STATIC_LIBS=ON \
#    -D PYTHON_ROOT_DIR=/SHARED/SOFTWARE/PYTHON-3.3.0 \
#    -D PYTHON_INCLUDE_DIR=/SHARED/SOFTWARE/PYTHON-3.3.0/INCLUDE/PYTHON3.3M \
#    -D PYTHON_INCLUDE_DIRS=/SHARED/SOFTWARE/PYTHON-3.3.0/INCLUDE/PYTHON3.3M \
#    -D PYTHON_LIBPATH=/shared/software/python-3.3.0/lib/ \
#    -D BOOST_ROOT=$BOOST \
#    -D PYTHON_INC:STRING=/usr/include/python3.1 \
#    -D PYTHON_LIB:STRING=python3.1 \
#    -D PYTHON_LIBPATH:STRING=/usr/lib/python3.1 \
#    -D FFMPEG:FILEPATH=/shared/software/ffmpeg \
#    -D FFMPEG_LIB:STRING="avformat;avcodec;avutil;avdevice;swscale;jack;theora;ogg;mp3lame;x264;vorbis;theoraenc;theoradec;vorbisenc;vorbisfile;xvidcore;bz2;asound" \

make -s -j $CORES install

#cd ../blender ; mv ../cmake_debug/bin/blender ./blender_debug

# END CMAKE

echo ""
echo "done, Ctrl+C to close"
sleep 2h
