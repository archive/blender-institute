#!/bin/bash

gnome_terminal=`which gnome-terminal`
if [[ -n $gnome_terminal ]] ; then
	$gnome_terminal --title "$1" -x bash -c "$2"
	exit
fi

xterm=`which xterm`
if [[ -n $xterm ]] ; then
	$xterm -title "$1" -rv -e bash -c "$2"
	exit
fi

konsole4=`which /usr/lib/kde4/bin/konsole`
if [[ -n $konsole4 ]] ; then
	$konsole4 -e bash -c "$2"
	exit
fi

konsole=`which konsole`
if [[ -n $konsole ]] ; then
	$konsole -e bash -c "$2"
	exit
fi

$2

