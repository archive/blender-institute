#!/bin/bash

mkdir -p /render
mkdir -p /shared

if [ -z "`cat /etc/fstab | grep /render`" ]; then
  echo "Writing fstab"
  echo "192.168.3.14:/extra/ltstor/data/shared     /shared     nfs    rw,soft,rsize=32768,wsize=32768,intr,async,noatime,nodiratime,tcp,auto    0    0" >> /etc/fstab
  echo "192.168.3.14:/extra/ststor                 /render     nfs    rw,soft,rsize=32768,wsize=32768,intr,async,noatime,nodiratime,tcp,auto    0    0" >> /etc/fstab
fi

apt-get install -y \
  nfs-common aptitude htop krita inkscape git cmake subversion \
  imagemagick vlc emacs vim-gtk emacs-goodies-el audacity openssh-server libreoffice gedit gcc-4.7 g++-4.7 mc \
  rapidsvn \
  libxvidcore-dev libvpx-dev libmp3lame-dev

apt-get -y purge \
  gnumeric \
  abiword \
  mousepad

# file assosiations
# NOT AS ROOT
# xdg-mime default gedit.desktop text/plain

mount -a

# -------------
# Network Edits

cat < EOF >> /etc/network/interfaces

auto eth0
allow-hotplug eth0
iface eth0 inet dhcp
    post-up mount -a
EOF

sudo dpkg --purge network-manager network-manager-gnome


# MANUAL CONFIG (could copy home dir about once done...)
# (important, window resize is horrible!)
# menu -> Appearance -> Style -> Adwaita
# menu -> Window Manager -> Style -> Moheli
# menu -> Window Manager tweaks -> Accessibility -> Key used ... (Use Hyper)
#                               -> Compositor -> Disable!
#
# menu -> Theme (Change pink color to dark blue)
# Move panel to bottom

echo "ALL DONE!"
